#ifndef __DRIVERS_MTD_W25QXXXJV_H
#define __DRIVERS_MTD_W25QXXXJV_H

/****************************************************************************
 * Name: w25qxxx_initialize
 *
 * Description:
 *   Create an initialized MTD device instance for the QuadSPI-based W25QxxxJV
 *   FLASH part from Winbond.
 *
 ****************************************************************************/

FAR struct mtd_dev_s *w25qxxxjv_initialize(FAR struct qspi_dev_s *qspi,
                                         bool unprotect);

#endif /* __DRIVERS_MTD_W25QXXXJV_H */

