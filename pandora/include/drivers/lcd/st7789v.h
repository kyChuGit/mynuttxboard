/****************************************************************************
 * include/nuttx/lcd/st7789v.h
 *
 *   Copyright (C) 2015 Omni Hoverboards Inc. All rights reserved.
 *   Author: Paul Alexander Patience <paul-a.patience@polymtl.ca>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __INCLUDE_NUTTX_LCD_ST7789V_H
#define __INCLUDE_NUTTX_LCD_ST7789V_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

/************/
#define CONFIG_LCD_ST7789V   1

#define CONFIG_ST7789V_XRES 240
#define CONFIG_ST7789V_YRES 240

#define CONFIG_ST7789V_SPI4WIRE
#define CONFIG_ST7789V_SPIMODE 0
#define CONFIG_ST7789V_SPIFREQ 42000000
/************/

#ifdef CONFIG_LCD_ST7789V

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Configuration ************************************************************/

/* ST7789V configuration settings:
 * CONFIG_ST7789V_PARALLEL8BIT - 8-bit parallel interface
 * CONFIG_ST7789V_SPI3WIRE     - 3-wire SPI interface
 * CONFIG_ST7789V_SPI4WIRE     - 4-wire SPI interface
 * CONFIG_ST7789V_SPIMODE      - SPI mode
 * CONFIG_ST7789V_SPIFREQ      - SPI frequency
 * CONFIG_ST7789V_NINTERFACES  - number of physical devices supported
 * CONFIG_ST7789V_XRES         - X resolution
 * CONFIG_ST7789V_YRES         - Y resolution
 * CONFIG_ST7789V_MIRRORX      - mirror along the X axis
 * CONFIG_ST7789V_MIRRORY      - mirror along the Y axis
 * CONFIG_ST7789V_INVERT       - invert the display
 * CONFIG_ST7789V_VDDEXT       - external VDD
 * CONFIG_ST7789V_TRST         - reset period
 * CONFIG_ST7789V_TPRECHG1     - first pre-charge period
 * CONFIG_ST7789V_PERFENHANCE  - enhace display performance
 * CONFIG_ST7789V_CLKDIV       - clock divider
 * CONFIG_ST7789V_OSCFREQ      - oscillator frequency
 * CONFIG_ST7789V_TPRECHG2     - second pre-charge period
 * CONFIG_ST7789V_VPRECHG      - pre-charge voltage level
 * CONFIG_ST7789V_VCOMH        - COM deselect voltage level
 * CONFIG_ST7789V_CONTRASTA    - color A contrast
 * CONFIG_ST7789V_CONTRASTB    - color B contrast
 * CONFIG_ST7789V_CONTRASTC    - color C contrast
 * CONFIG_ST7789V_MSTRCONTRAST - master contrast ratio
 *
 * Required LCD driver settings:
 * CONFIG_LCD_ST7789V          - enables ST7789V support
 * CONFIG_LCD_MAXPOWER         - maximum power, must be 1
 *
 * Additional LCD driver settings:
 * CONFIG_LCD_LANDSCAPE        - landscape
 * CONFIG_LCD_RLANDSCAPE       - reverse landscape
 * CONFIG_LCD_PORTRAIT         - portrait
 * CONFIG_LCD_RPORTRAIT        - reverse portrait
 *
 * Required SPI driver settings:
 * CONFIG_SPI                  - enables support for SPI
 * CONFIG_SPI_CMDDATA          - enables support for cmd/data selection
 *                               (if using 4-wire SPI)
 *
 * NX settings that must be undefined:
 * CONFIG_NX_DISABLE_16BPP     - disables 16 bpp support
 */

/* Fundamental Commands *****************************************************/

/* Sleep Out */
#define ST7789V_CMD_SLPOUT       0x11

/* Display Inversion On */
#define ST7789V_CMD_INVON        0x21

/* Set display off (sleep mode on).  No data bytes. */
#define ST7789V_CMD_DISPOFF      0x28

/* Set display on (sleep mode off).  No data bytes. */
#define ST7789V_CMD_DISPON       0x29

/* Set column address. Four data bytes.
 *   XS[15:0]
 *   XE[15:0]
 */
#define ST7789V_CMD_COLADDR      0x2A

/* Set row address. Four data bytes.
 *   YS[15:0]
 *   YE[15:0]
 */
#define ST7789V_CMD_ROWADDR      0x2B

/* Write data bytes to RAM. */
#define ST7789V_CMD_RAMWRITE     0x2C

/* Read data bytes from RAM. */
#define ST7789V_CMD_RAMREAD      0x2E

/* Memory Data Access Control */
#define ST7789V_CMD_MADCTL       0x36

/* Interface Pixel Format */
#define ST7789V_CMD_COLMOD       0x3A

/* Porch Setting */
#define ST7789V_CMD_PORCTRL      0xB2

/* Gate Control */
#define ST7789V_CMD_GCTRL        0xB7

/* VCOM Setting */
#define ST7789V_CMD_VCOMS        0xBB

/* LCM Control */
#define ST7789V_CMD_LCMCTRL      0xC0

/* VDV and VRH Command Enable */
#define ST7789V_CMD_VDVVRHEN     0xC2

/* VRH Set */
#define ST7789V_CMD_VRHS         0xC3

/* VDV Set */
#define ST7789V_CMD_VDVS         0xC4

/* Frame Rate Control in Normal Mode */
#define ST7789V_CMD_FRCTRL2      0xC6

/* Power Control 1 */
#define ST7789V_CMD_PWCTRL1      0xD0

/* Positive Voltage Gamma Control */
#define ST7789V_CMD_PVGAMCTRL    0xE0

/* Negative Voltage Gamma Control */
#define ST7789V_CMD_NVGAMCTRL    0xE1

/****************************************************************************
 * Public Types
 ****************************************************************************/

#ifdef CONFIG_ST7789V_PARALLEL8BIT
struct st7789v_lcd_s
{
  void    (*cmd)(FAR struct st7789v_lcd_s *lcd, uint8_t cmd);
#ifndef CONFIG_LCD_NOGETRUN
  uint8_t (*read)(FAR struct st7789v_lcd_s *lcd);
#endif
  void    (*write)(FAR struct st7789v_lcd_s *lcd, uint8_t data);
};
#elif defined(CONFIG_ST7789V_SPI3WIRE) || defined(CONFIG_ST7789V_SPI4WIRE)
struct spi_dev_s;
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

/****************************************************************************
 * Name: st7789v_initialize
 *
 * Description:
 *   Initialize the video hardware.  The initial state of the device
 *   is fully initialized, display memory cleared, and ready to use,
 *   but with the power setting at 0 (full off == sleep mode).
 *
 * Input Parameters:
 *   lcd   - A reference to the platform-specific interface.
 *   spi   - A reference to the SPI driver instance.
 *   devno - A value in the range of 0 through CONFIG_ST7789V_NINTERFACES-1.
 *           This allows support for multiple devices.
 *
 * Returned Value:
 *   On success, this function returns a reference to the LCD object for the
 *   specified device.  NULL is returned on failure.
 *
 ****************************************************************************/

#ifdef CONFIG_ST7789V_PARALLEL8BIT
FAR struct lcd_dev_s *st7789v_initialize(FAR struct st7789v_lcd_s *lcd,
                                         unsigned int devno);
#elif defined(CONFIG_ST7789V_SPI3WIRE) || defined(CONFIG_ST7789V_SPI4WIRE)
FAR struct lcd_dev_s *st7789v_initialize(FAR struct spi_dev_s *spi,
                                         unsigned int devno);
#endif

#ifdef __cplusplus
}
#endif

#endif /* CONFIG_LCD_ST7789V */
#endif /* __INCLUDE_NUTTX_LCD_ST7789V_H */
