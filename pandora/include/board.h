/****************************************************************************
 * myboards/pandora/include/board.h
 *
 *   Copyright (C) 2017 Gregory Nutt. All rights reserved.
 *   Author: Simon Piriou <spiriou31@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __MYBOARDS_PANDORA_INCLUDE_BOARD_H
#define __MYBOARDS_PANDORA_INCLUDE_BOARD_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#ifndef __ASSEMBLY__
#  include <stdbool.h>
#endif

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Clocking *****************************************************************/

#include <arch/board/pandora_clock.h>

/* DMA Channel/Stream Selections ********************************************/

/* Stream selections are arbitrary for now but might become important in the future
 * is we set aside more DMA channels/streams.
 */

/* Values defined in arch/arm/src/stm32l4/hardware/stm32l4x5xx_dma.h */

// DMACHAN_SPI1_RX -> DMA1CHAN2  <2 choices>
// DMACHAN_SPI1_TX -> DMA1CHAN3  <2 choices>
#define DMACHAN_SPI1_RX DMACHAN_SPI1_RX_1 /* 2 choices */
#define DMACHAN_SPI1_TX DMACHAN_SPI1_TX_1 /* 2 choices */

// DMACHAN_SPI2_RX -> DMA1CHAN4
// DMACHAN_SPI2_TX -> DMA1CHAN5

// DMACHAN_SPI3_RX -> DMA2CHAN1
// DMACHAN_SPI3_TX -> DMA2CHAN2

// DMACHAN_QSPI    -> DMA2CHAN7

/* UART RX DMA configurations */
// DMACHAN_USART1_RX -> DMA2CHAN7
// DMACHAN_USART1_TX -> DMA2CHAN6
//#define DMACHAN_USART1_RX DMACHAN_USART1_RX_2
#define DMACHAN_USART1_TX DMACHAN_USART1_TX_2

/* LED definitions **********************************************************/

/* LEDs
 *
 * Two user LEDs are available on pins PA5 and PB14.
 */

/* LED index values for use with board_userled() */

#define BOARD_LED_BLUE    0
#define BOARD_LED_GREEN   1
#define BOARD_LED_RED     2
#define BOARD_NLEDS       3

/* LED bits for use with board_userled_all() */

#define BOARD_LEDR_BIT    (1 << BOARD_LED_RED)
#define BOARD_LEDG_BIT    (1 << BOARD_LED_GREEN)
#define BOARD_LEDB_BIT    (1 << BOARD_LED_BLUE)

/* If CONFIG_ARCH_LEDS is defined, the usage by the board port is defined in
 * include/board.h and src/stm32_leds.c. The LEDs are used to encode OS-related
 * events as follows:
 *
 *
 *   SYMBOL                     Meaning                      LED state
 *                                                        Red   Green Blue
 *   ----------------------  --------------------------  ------ ------ ----*/

#define LED_STARTED        0 /* NuttX has been started   OFF    OFF   OFF  */
#define LED_HEAPALLOCATE   1 /* Heap has been allocated  OFF    OFF   ON   */
#define LED_IRQSENABLED    2 /* Interrupts enabled       OFF    ON    OFF  */
#define LED_STACKCREATED   3 /* Idle stack created       OFF    ON    ON   */
#define LED_INIRQ          4 /* In an interrupt          N/C    N/C   GLOW */
#define LED_SIGNAL         5 /* In a signal handler      N/C    GLOW  N/C  */
#define LED_ASSERTION      6 /* An assertion failed      GLOW   N/C   GLOW */
#define LED_PANIC          7 /* The system has crashed   Blink  OFF   N/C  */
#define LED_IDLE           8 /* MCU is is sleep mode     ON     OFF   OFF  */

/* Thus if the Green LED is statically on, NuttX has successfully booted and
 * is, apparently, running normally.  If the Red LED is flashing at
 * approximately 2Hz, then a fatal error has been detected and the system
 * has halted.
 */

/* Buttons
 *
 *  There are 4 buttons
 *  connected to PD10, PD9, PD8, PC13
 *                K0    K1   K2  WAKE_UP
 */
#define BUTTON_LEFT        0
#define BUTTON_DOWN        1
#define BUTTON_RIGHT       2
#define BUTTON_UP          3
#define NUM_BUTTONS        4

#define BUTTON_LEFT_BIT    (1 << BUTTON_LEFT)
#define BUTTON_DOWN_BIT    (1 << BUTTON_DOWN)
#define BUTTON_RIGHT_BIT   (1 << BUTTON_RIGHT)
#define BUTTON_UP_BIT      (1 << BUTTON_UP)

/* Alternate function pin selections ****************************************/

/* USART1: Connected to STLink Debug via PA9, PA10 */

#define GPIO_USART1_RX GPIO_USART1_RX_1
#define GPIO_USART1_TX GPIO_USART1_TX_1

/* SPI */

#define GPIO_SPI1_MISO   (GPIO_SPI1_MISO_1 | GPIO_SPEED_100MHz)     /* PA6 */
#define GPIO_SPI1_MOSI   (GPIO_SPI1_MOSI_1 | GPIO_SPEED_100MHz)     /* PA7 */
#define GPIO_SPI1_SCK    (GPIO_SPI1_SCK_1 | GPIO_SPEED_100MHz)      /* PA5 */

#define GPIO_SPI2_SCK    (GPIO_SPI2_SCK_2 | GPIO_SPEED_100MHz)      /* PB13 */
#define GPIO_SPI2_MISO   (GPIO_SPI2_MISO_1 | GPIO_SPEED_100MHz)     /* PB14 */
#define GPIO_SPI2_MOSI   (GPIO_SPI2_MOSI_1 | GPIO_SPEED_100MHz)     /* PB15 */

#define GPIO_SPI3_MISO   (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_100MHz | \
                          GPIO_OUTPUT_CLEAR | GPIO_PORTB | GPIO_PIN4) /* PB4 for LCD_WR */
#define GPIO_SPI3_MOSI   (GPIO_SPI3_MOSI_1 | GPIO_SPEED_100MHz)       /* PB5 */
#define GPIO_SPI3_SCK    (GPIO_SPI3_SCK_1 | GPIO_SPEED_100MHz)        /* PB3 */

/* PWM output for full bridge, uses config 1
 * TIM4_CH2     | PB7 (for LCD_PWR)
 * LPTIM1_CH1   | PB2 (for Buzzer)
 * TIM5_CH1     | PA0 (MOTOR_CTRL_B)
 * TIM5_CH2     | PA1 (MOTOR_CTRL_A)
 */
#define GPIO_TIM4_CH2OUT  GPIO_TIM4_CH2OUT_1

#define GPIO_TIM5_CH1OUT  GPIO_TIM5_CH1OUT_1
#define GPIO_TIM5_CH2OUT  GPIO_TIM5_CH2OUT_1

#define GPIO_LPTIM1_CH1OUT GPIO_LPTIM1_OUT_1

/* Quad SPI: connected to W25Q128JVSQ external flash memory */

#define GPIO_QSPI_CS  (GPIO_QSPI_NCS_2     | GPIO_FLOAT | GPIO_PUSHPULL | GPIO_SPEED_100MHz)
#define GPIO_QSPI_IO0 (GPIO_QSPI_BK1_IO0_2 | GPIO_FLOAT | GPIO_PUSHPULL | GPIO_SPEED_100MHz)
#define GPIO_QSPI_IO1 (GPIO_QSPI_BK1_IO1_2 | GPIO_FLOAT | GPIO_PUSHPULL | GPIO_SPEED_100MHz)
#define GPIO_QSPI_IO2 (GPIO_QSPI_BK1_IO2_2 | GPIO_FLOAT | GPIO_PUSHPULL | GPIO_SPEED_100MHz)
#define GPIO_QSPI_IO3 (GPIO_QSPI_BK1_IO3_2 | GPIO_FLOAT | GPIO_PUSHPULL | GPIO_SPEED_100MHz)
#define GPIO_QSPI_SCK (GPIO_QSPI_CLK_2     | GPIO_FLOAT | GPIO_PUSHPULL | GPIO_SPEED_100MHz)

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif  /* __MYBOARDS_PANDORA_INCLUDE_BOARD_H */
