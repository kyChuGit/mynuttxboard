/****************************************************************************
 * myboards/pandora/src/pandora.h
 *
 *   Copyright (C) 2017, 2019 Gregory Nutt. All rights reserved.
 *   Author: Simon Piriou <spiriou31@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __MYBOARDS_PANDORA_SRC_PANDORA_H
#define __MYBOARDS_PANDORA_SRC_PANDORA_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/compiler.h>

#include <arch/stm32l4/chip.h>

#include "stm32l4_gpio.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Configuration ************************************************************/

#define HAVE_W25Q128JVSQ 1
#define HAVE_W25Q128JVSQ_SMARTFS 1

#if !defined(CONFIG_MTD_W25QXXXJV) || !defined(CONFIG_STM32L4_QSPI)
#  undef HAVE_W25Q128JVSQ
#endif

#if !defined(HAVE_W25Q128JVSQ) || !defined(CONFIG_MTD_SMART) || \
    !defined(CONFIG_FS_SMARTFS)
#  undef HAVE_W25Q128JVSQ_SMARTFS
#endif

#define HAVE_MMCSD_SPI      1

/* GPIO Definitions *********************************************************/

/* LEDs */

#define GPIO_LEDR        (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz |\
                          GPIO_OUTPUT_CLEAR | GPIO_PORTE | GPIO_PIN7)
#define GPIO_LEDG        (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz |\
                          GPIO_OUTPUT_CLEAR | GPIO_PORTE | GPIO_PIN8)
#define GPIO_LEDB        (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz |\
                          GPIO_OUTPUT_CLEAR | GPIO_PORTE | GPIO_PIN9)

/* Buttons
 *
 *  There are 4 buttons
 *  connected to PD10, PD9, PD8, PC13
 *                K0    K1   K2  WAKE_UP
 */

#define MIN_IRQBUTTON   BUTTON_LEFT
#define MAX_IRQBUTTON   BUTTON_UP
#define NUM_IRQBUTTONS  4

#define GPIO_BTN_LEFT \
  (GPIO_INPUT |GPIO_PULLUP |GPIO_EXTI | GPIO_PORTD | GPIO_PIN8)
#define GPIO_BTN_DOWN \
  (GPIO_INPUT |GPIO_PULLUP |GPIO_EXTI | GPIO_PORTD | GPIO_PIN9)
#define GPIO_BTN_RIGHT \
  (GPIO_INPUT |GPIO_PULLUP |GPIO_EXTI | GPIO_PORTD | GPIO_PIN10)
#define GPIO_BTN_UP \
  (GPIO_INPUT |GPIO_PULLDOWN |GPIO_EXTI | GPIO_PORTC | GPIO_PIN13)

/* SPI chip selects */

#ifdef CONFIG_MMCSD_SPI
#  define GPIO_SDCARD_CS \
  (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_SET | GPIO_PORTC | GPIO_PIN3)
#endif

#define GPIO_SPI2_CS \
  (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_SET | GPIO_PORTD | GPIO_PIN5)

#define GPIO_LCD_CS \
  (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_SET | GPIO_PORTD | GPIO_PIN7)
#define GPIO_LCD_WR \
  (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_CLEAR | GPIO_PORTB | GPIO_PIN4) /* PB4 for LCD_WR */
#define GPIO_LCD_RST \
  (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_CLEAR | GPIO_PORTB | GPIO_PIN6) /* PB6 for LCD_RST */

/****************************************************************************
 * Public data
 ****************************************************************************/

#ifndef __ASSEMBLY__

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32l4_bringup
 *
 * Description:
 *   Called either by board_initialize() if CONFIG_BOARD_LATE_INITIALIZE or by
 *   board_app_initialize if CONFIG_LIB_BOARDCTL is selected.  This function
 *   initializes and configures all on-board features appropriate for the
 *   selected configuration.
 *
 ****************************************************************************/

#if defined(CONFIG_LIB_BOARDCTL) || defined(CONFIG_BOARD_LATE_INITIALIZE)
int stm32l4_bringup(void);
#endif

/****************************************************************************
 * Name: stm32l4_spidev_initialize
 *
 * Description:
 *   Called to configure SPI chip select GPIO pins for the Nucleo-F401RE and
 *   Nucleo-F411RE boards.
 *
 ****************************************************************************/

#if defined(CONFIG_STM32L4_SPI1) || defined(CONFIG_STM32L4_SPI2) || \
    defined(CONFIG_STM32L4_SPI3)
void stm32l4_spidev_initialize(void);
#endif

/****************************************************************************
 * Name: stm32l4_w25q128jv_initialize
 *
 * Description:
 *   Initializes QSPI-based serial NOR FLASH
 *
 ****************************************************************************/

#ifdef HAVE_W25Q128JVSQ
int stm32l4_w25q128jv_initialize(void);
#endif /* HAVE_W25Q128JVSQ */

/****************************************************************************
 * Name: stm32l4_mmcsd_initialize
 *
 * Description:
 *   Initializes SPI-based SD card
 *
 ****************************************************************************/

#ifdef CONFIG_MMCSD_SPI
int stm32l4_mmcsd_initialize(int minor);
#endif

/****************************************************************************
 * Name: stm32l4_pwm_setup
 *
 * Description:
 *   Initialize PWM and register the PWM device.
 *
 ****************************************************************************/

#ifdef CONFIG_PWM
int stm32l4_pwm_setup(void);
#endif

/****************************************************************************
 * Name: stm32_tone_setup
 *
 * Description:
 *   Function used to initialize a PWM and Oneshot timers to Audio Tone Generator.
 *
 ****************************************************************************/

#ifdef CONFIG_AUDIO_TONE
int stm32_tone_setup(void);
#endif

/****************************************************************************
 * Name: stm32_timer_driver_setup
 *
 * Description:
 *   Configure the timer drivers.
 *
 * Input Parameters:
 *   None
 *
 * Returned Value:
 *   Zero (OK) is returned on success; A negated errno value is returned
 *   to indicate the nature of any failure.
 *
 ****************************************************************************/

#ifdef CONFIG_TIMER
int stm32l4_timer_driver_setup(void);
#endif

#endif /* __ASSEMBLY__ */
#endif /* __BOARDS_ARM_STM32L4_B_L475E_IOT01A_SRC_B_L475E_IOT01A_H */
