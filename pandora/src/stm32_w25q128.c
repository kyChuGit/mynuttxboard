/****************************************************************************
 * myboards/pandora/src/stm32_w25q128.c
 *
 *   Copyright (C) 2016 Uniquix Tecnologia. All rights reserved.
 *   Author: kyChu <kychu@qq.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdio.h>
#include <stdbool.h>
#include <debug.h>
#include <errno.h>

#include <stdlib.h>
#include <sys/types.h>
#include <syslog.h>

#include <nuttx/arch.h>
#include <nuttx/board.h>

#include <nuttx/spi/qspi.h>
#include <nuttx/mtd/mtd.h>

#include "up_arch.h"
#include "up_internal.h"
#include "stm32l4_qspi.h"

#include "pandora.h"

#ifdef HAVE_W25Q128JVSQ

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/*****************************************************************************
 * Name: stm32l4_w25q128jv_initialize
 *
 * Description:
 *   Initialize w25qxxxjv and smartfs
 *
 ****************************************************************************/
int stm32l4_w25q128jv_initialize(void)
{
  int ret = OK;
  /* Create an instance of the STM32L4 QSPI device driver */

  FAR struct qspi_dev_s *g_qspi;
  FAR struct mtd_dev_s *g_mtd_fs;

  g_qspi = stm32l4_qspi_initialize(0);
  if (g_qspi == NULL) {
    syslog(LOG_ERR, "ERROR: stm32l4_qspi_initialize failed\n");
    return -EIO;
  }

  /* Use the QSPI device instance to initialize the
   * n25qxxx flash device.
   */

  g_mtd_fs = w25qxxxjv_initialize(g_qspi, true);
  if (!g_mtd_fs) {
    syslog(LOG_ERR, "ERROR: w25qxxxjv_initialize failed\n");
    return -EIO;
  }

#ifdef CONFIG_PANDORA_MTD_PART
  {
    /* Create partitions on external flash memory */

    int partno;
    int partsize;
    int partoffset;
    int partszbytes;
    int erasesize;
    FAR struct mtd_geometry_s geo;
    const char *ptr = CONFIG_PANDORA_MTD_PART_LIST;
    FAR struct mtd_dev_s *mtd_part;
    char  partref[4];

    /* Now create a partition on the FLASH device */

    partno = 0;
    partoffset = 0;

    /* Query MTD geometry */

    ret = MTD_IOCTL(g_mtd_fs, MTDIOC_GEOMETRY, (unsigned long)(uintptr_t)&geo);
    if (ret < 0) {
      syslog(LOG_ERR, "ERROR: MTDIOC_GEOMETRY failed\n");
      return ret;
    }

    /* Get the Flash erase size */

    erasesize = geo.erasesize;

    while (*ptr != '\0') {
      /* Get the partition size */

      partsize = atoi(ptr);
      if (partsize <= 0) {
        syslog(LOG_ERR, "Error while processing <%s>\n", ptr);
        goto process_next_part;
      }

      partszbytes = (partsize << 10); /* partsize is defined in KB */

      if ((partszbytes < erasesize) || ((partszbytes % erasesize) != 0)) {
        syslog(LOG_ERR, "Invalid partition size: %d bytes\n", partszbytes);
        partszbytes = (partszbytes+erasesize) & -erasesize;
      }

      mtd_part = mtd_partition(g_mtd_fs, partoffset, partszbytes / geo.blocksize);
      partoffset += partszbytes / geo.blocksize;

      if (!mtd_part) {
        syslog(LOG_ERR, "Failed to create part %d, size=%d\n", partno, partsize);
        goto process_next_part;
      }

#if defined(CONFIG_MTD_SMART) && defined(CONFIG_FS_SMARTFS)
      /* Now initialize a SMART Flash block device and bind it to the MTD device */
      
      sprintf(partref, "p%d", partno);
      smart_initialize(CONFIG_PANDORA_MTD_FLASH_MINOR, mtd_part, partref);
#endif

process_next_part:
      /* Update the pointer to point to the next size in the list */

      while ((*ptr >= '0') && (*ptr <= '9')) {
        ptr++;
      }

      if (*ptr == ',') {
        ptr++;
      }

      /* Increment the part number */

      partno++;
    }
  }
#else /* CONFIG_PANDORA_MTD_PART */

#ifdef HAVE_W25Q128JVSQ_SMARTFS
  /* Configure the device with no partition support */

  smart_initialize(CONFIG_PANDORA_MTD_FLASH_MINOR, g_mtd_fs, NULL);
#endif /* HAVE_W25Q128JVSQ_SMARTFS */

#endif /* CONFIG_PANDORA_MTD_PART */

  return ret;
}

#endif /* HAVE_W25Q128JVSQ */

