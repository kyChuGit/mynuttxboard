/****************************************************************************
 * drivers/lcd/st7789v.c
 * LCD driver for the Sitronix ST7789V LCD controller
 *
 *   Copyright (C) 2015 Omni Hoverboards Inc. All rights reserved.
 *   Author: kyChu <kyChu@qq.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/arch.h>
#include <nuttx/spi/spi.h>
#include <nuttx/lcd/lcd.h>
//#include <nuttx/lcd/st7789v.h>
#include <arch/board/drivers/lcd/st7789v.h>

#ifdef CONFIG_LCD_ST7789V

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Max power */

#if CONFIG_LCD_MAXPOWER != 1
#  error "CONFIG_LCD_MAXPOWER should be 1"
#endif

/* 9-bit SPI */

#ifdef CONFIG_ST7789V_SPI3WIRE
#  define ST7789V_SPICMD  0
#  define ST7789V_SPIDATA (1 << 8)
#  define ST7789V_SPIBITS 9
#else
#  define ST7789V_SPIBITS 8
#endif

/* Macro Helpers ************************************************************/

/* Display resolution */

#if defined(CONFIG_LCD_LANDSCAPE) || defined(CONFIG_LCD_RLANDSCAPE)
#define ST7789V_XRES         CONFIG_ST7789V_XRES
#define ST7789V_YRES         CONFIG_ST7789V_YRES
#else
#define ST7789V_XRES         CONFIG_ST7789V_YRES
#define ST7789V_YRES         CONFIG_ST7789V_XRES
#endif

/* Color depth and format */

#define ST7789V_BPP          16
#define ST7789V_COLORFMT     FB_FMT_RGB16_565
#define ST7789V_STRIDE       (2 * ST7789V_XRES)
#define ST7789V_PIX2BYTES(p) (2 * (p))

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* This structure describes the state of this driver */

struct st7789v_dev_s
{
  /* Publically visible device structure */

  struct lcd_dev_s          dev;

  /* Private LCD-specific information follows */

#ifdef CONFIG_ST7789V_PARALLEL8BIT
  FAR struct st7789v_lcd_s *lcd;   /* Contained platform-specific interface */
#elif defined(CONFIG_ST7789V_SPI3WIRE) || defined(CONFIG_ST7789V_SPI4WIRE)
  FAR struct spi_dev_s     *spi;   /* Contained SPI driver instance */
#endif
  uint8_t                   power; /* Current power (backlight) setting */

  /* This is working memory allocated by the LCD driver for each LCD device
   * and for each color plane.  This memory will hold one raster line of
   * data.  The size of the allocated run buffer must therefore be at least
   * (bpp * xres / 8).  Actual alignment of the buffer must conform to the
   * bitwidth of the underlying pixel type.
   *
   * If there are multiple planes, they may share the same working buffer
   * because different planes will not be operate on concurrently.  However,
   * if there are multiple LCD devices, they must each have unique run
   * buffers.
   */

  uint16_t                  runbuffer[ST7789V_XRES];

  /* bytes swap buffer write to LCD */
  uint8_t                  linebuffer[ST7789V_PIX2BYTES(ST7789V_XRES)];

  /* This is another buffer, but used internally by the LCD driver in order
   * to expand the pixel data into 9-bit data needed by the LCD.  There are
   * some customizations that would eliminate the need for this extra buffer
   * and for the extra expansion/copy, but those customizations would require
   * a special, non-standard SPI driver that could expand 8- to 9-bit data on
   * the fly.
   */

#ifdef CONFIG_ST7789V_SPI3WIRE
  uint16_t                  rowbuffer[ST7789V_STRIDE+1];
#endif
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Helpers */

#ifdef CONFIG_ST7789V_PARALLEL8BIT
#define st7789v_select(priv)
#define st7789v_deselect(priv)
#elif defined(CONFIG_ST7789V_SPI3WIRE) || defined(CONFIG_ST7789V_SPI4WIRE)
static void st7789v_select(FAR struct st7789v_dev_s *priv);
static void st7789v_deselect(FAR struct st7789v_dev_s *priv);
#endif

#if defined(CONFIG_ST7789V_PARALLEL8BIT) && !defined(CONFIG_LCD_NOGETRUN)
static void st7789v_read(FAR struct st7789v_dev_s *priv, uint8_t cmd,
                         FAR uint8_t *data, size_t datlen);
#endif
static void st7789v_write(FAR struct st7789v_dev_s *priv, uint8_t cmd,
                          FAR const uint8_t *data, size_t datlen);

/* LCD Data Transfer Methods */

static int st7789v_putrun(fb_coord_t row, fb_coord_t col,
                          FAR const uint8_t *buffer, size_t npixels);
static int st7789v_getrun(fb_coord_t row, fb_coord_t col,
                          FAR uint8_t *buffer, size_t npixels);

/* LCD Configuration */

static int st7789v_getvideoinfo(FAR struct lcd_dev_s *dev,
                                FAR struct fb_videoinfo_s *vinfo);
static int st7789v_getplaneinfo(FAR struct lcd_dev_s *dev,
                                unsigned int planeno,
                                FAR struct lcd_planeinfo_s *pinfo);

/* LCD RGB Mapping */

#ifdef CONFIG_FB_CMAP
#  error "RGB color mapping not supported by this driver"
#endif

/* Cursor Controls */

#ifdef CONFIG_FB_HWCURSOR
#  error "Cursor control not supported by this driver"
#endif

/* LCD Specific Controls */

static int st7789v_getpower(struct lcd_dev_s *dev);
static int st7789v_setpower(struct lcd_dev_s *dev, int power);
static int st7789v_getcontrast(struct lcd_dev_s *dev);
static int st7789v_setcontrast(struct lcd_dev_s *dev, unsigned int contrast);

/* Initialization */

static inline void st7789v_hwinitialize(FAR struct st7789v_dev_s *priv);

/****************************************************************************
 * Private Data
 ****************************************************************************/

/* This is the standard, NuttX LCD driver object */

static struct st7789v_dev_s g_lcddev;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: st7789v_select
 *
 * Description:
 *   Select the SPI, locking and re-configuring if necessary.
 *
 ****************************************************************************/

#if defined(CONFIG_ST7789V_SPI3WIRE) || defined(CONFIG_ST7789V_SPI4WIRE)
static void st7789v_select(FAR struct st7789v_dev_s *priv)
{
  FAR struct spi_dev_s *spi = priv->spi;

  /* Select the chip, locking the SPI bus in case there are multiple devices
   * competing for the SPI bus
   */

  ginfo("SELECTED\n");

  SPI_LOCK(spi, true);
  SPI_SELECT(spi, SPIDEV_DISPLAY(0), true);

  /* Now make sure that the SPI bus is configured for this device (it might
   * have gotten configured for a different device while unlocked)
   */

  SPI_SETMODE(spi, CONFIG_ST7789V_SPIMODE);
  SPI_SETBITS(spi, ST7789V_SPIBITS);
  (void)SPI_HWFEATURES(spi, 0);
  (void)SPI_SETFREQUENCY(spi, CONFIG_ST7789V_SPIFREQ);
}
#endif

/****************************************************************************
 * Name: st7789v_deselect
 *
 * Description:
 *   De-select the SPI.
 *
 ****************************************************************************/

#if defined(CONFIG_ST7789V_SPI3WIRE) || defined(CONFIG_ST7789V_SPI4WIRE)
static void st7789v_deselect(FAR struct st7789v_dev_s *priv)
{
  FAR struct spi_dev_s *spi = priv->spi;

  /* De-select the chip and relinquish the SPI bus */

  ginfo("DE-SELECTED\n");

  SPI_SELECT(spi, SPIDEV_DISPLAY(0), false);
  SPI_LOCK(spi, false);
}
#endif

/****************************************************************************
 * Name: st7789v_read
 *
 * Description:
 *   Send a 1-byte command and read datlen data bytes.
 *
 ****************************************************************************/

#if defined(CONFIG_ST7789V_PARALLEL8BIT) && !defined(CONFIG_LCD_NOGETRUN)
static void st7789v_read(FAR struct st7789v_dev_s *priv, uint8_t cmd,
                         FAR uint8_t *data, size_t datlen)
{
  FAR struct st7789v_lcd_s *lcd = priv->lcd;
  size_t i;

  /* Sanity check */

  DEBUGASSERT(priv != NULL);
  DEBUGASSERT((data == NULL && datlen == 0) || (data != NULL && datlen > 0));

  /* Send the command */

  lcd->cmd(lcd, cmd);

  /* Discard the first data read if reading from the display */

  if (cmd == ST7789V_CMD_RAMREAD)
    {
      (void)lcd->read(lcd);
    }

  /* Read all of the data */

  for (i = 0; i < datlen; i++)
    {
      data[i] = lcd->read(lcd);
    }
}
#endif

/****************************************************************************
 * Name: st7789v_write
 *
 * Description:
 *   Send a 1-byte command followed by datlen data bytes.
 *
 ****************************************************************************/

#ifdef CONFIG_ST7789V_PARALLEL8BIT
static void st7789v_write(FAR struct st7789v_dev_s *priv, uint8_t cmd,
                          FAR const uint8_t *data, size_t datlen)
{
  FAR struct st7789v_lcd_s *lcd = priv->lcd;
  size_t i;

  /* Sanity check */

  DEBUGASSERT(priv != NULL);
  DEBUGASSERT((data == NULL && datlen == 0) || (data != NULL && datlen > 0));

  /* Send the command */

  lcd->cmd(lcd, cmd);

  /* Write all of the data */

  for (i = 0; i < datlen; i++)
    {
      lcd->write(lcd, data[i]);
    }
}
#elif defined(CONFIG_ST7789V_SPI3WIRE)
static void st7789v_write(FAR struct st7789v_dev_s *priv, uint8_t cmd,
                          FAR const uint8_t *data, size_t datlen)
{
  size_t i;

  /* Sanity check */

  DEBUGASSERT(priv != NULL);
  DEBUGASSERT((data == NULL && datlen == 0) || (data != NULL && datlen > 0));
  DEBUGASSERT(datlen <= ST7789V_STRIDE);

  /* Copy the command into the line buffer */

  priv->rowbuffer[0] = (uint16_t)cmd | ST7789V_SPICMD;

  /* Copy any data after the command into the line buffer */

  for (i = 0; i < datlen; i++)
    {
      priv->rowbuffer[i+1] = (uint16_t)data[i] | ST7789V_SPIDATA;
    }

  /* Send the line buffer */

  (void)SPI_SNDBLOCK(priv->spi, priv->rowbuffer, datlen+1);
}
#elif defined(CONFIG_ST7789V_SPI4WIRE)
static void st7789v_write(FAR struct st7789v_dev_s *priv, uint8_t cmd,
                          FAR const uint8_t *data, size_t datlen)
{
  FAR struct spi_dev_s *spi = priv->spi;

  /* Sanity check */

  DEBUGASSERT(priv != NULL);
  DEBUGASSERT((data == NULL && datlen == 0) || (data != NULL && datlen > 0));

  /* Select command transfer */

  (void)SPI_CMDDATA(spi, SPIDEV_DISPLAY(0), true);

  /* Send the command */

  (void)SPI_SEND(spi, cmd);

  /* Do we have any data to send? */

  if (datlen > 0)
    {
      /* Yes, select data transfer */

      (void)SPI_CMDDATA(spi, SPIDEV_DISPLAY(0), false);

      /* Transfer all of the data */

      (void)SPI_SNDBLOCK(spi, data, datlen);
    }
}
#endif

/****************************************************************************
 * Name: st7789v_setcursor
 *
 * Description:
 *   Set the cursor position.
 *
 ****************************************************************************/

static void st7789v_setcursor(FAR struct st7789v_dev_s *priv, uint16_t col,
                              uint16_t row)
{
  uint8_t buf[4];

#if defined(CONFIG_LCD_LANDSCAPE) || defined(CONFIG_LCD_RLANDSCAPE)
  /* Set the column address to the column */

  buf[0] = col >> 8;
  buf[1] = col;
  buf[2] = (ST7789V_XRES - 1) >> 8;
  buf[3] = ST7789V_XRES - 1;
  st7789v_write(priv, ST7789V_CMD_COLADDR, buf, 4);

  /* Set the row address to the row */

  buf[0] = row >> 8;
  buf[1] = row;
  buf[2] = (ST7789V_YRES - 1) >> 8;
  buf[3] = ST7789V_YRES - 1;
  st7789v_write(priv, ST7789V_CMD_ROWADDR, buf, 4);
#elif defined(CONFIG_LCD_PORTRAIT) || defined(CONFIG_LCD_RPORTRAIT)
  /* Set the column address to the row */

  buf[0] = row;
  buf[1] = ST7789V_YRES - 1;
  st7789v_write(priv, ST7789V_CMD_COLADDR, buf, 2);

  /* Set the row address to the column */

  buf[0] = col;
  buf[1] = ST7789V_XRES - 1;
  st7789v_write(priv, ST7789V_CMD_ROWADDR, buf, 2);
#endif
}

/****************************************************************************
 * Name: st7789v_putrun
 *
 * Description:
 *   This method can be used to write a partial raster line to the LCD:
 *
 * Input Parameters:
 *   row     - Starting row to write to (range: 0 <= row < yres)
 *   col     - Starting column to write to (range: 0 <= col <= xres-npixels
 *   buffer  - The buffer containing the run to be written to the LCD
 *   npixels - The number of pixels to write to the LCD
 *             (range: 0 < npixels <= xres-col)
 *
 ****************************************************************************/

static int st7789v_putrun(fb_coord_t row, fb_coord_t col,
                          FAR const uint8_t *buffer, size_t npixels)
{
  size_t n;
  FAR struct st7789v_dev_s *priv = &g_lcddev;

  /* Sanity check */

  DEBUGASSERT(buffer != NULL && ((uintptr_t)buffer & 1) == 0 &&
              col >= 0 && col+npixels <= ST7789V_XRES &&
              row >= 0 && row < ST7789V_YRES);

  /* byte swap, from little-endian to big-endian */
  for(n = 0; n < npixels * 2; n += 2) {
	  priv->linebuffer[n] = buffer[n + 1];
	  priv->linebuffer[n + 1] = buffer[n];
  }

  /* Select and lock the device */

  st7789v_select(priv);

  /* Set the starting position for the run */

  st7789v_setcursor(priv, col, row);

  /* Write all of the data */

  st7789v_write(priv, ST7789V_CMD_RAMWRITE, (FAR const uint8_t *)priv->linebuffer,
                ST7789V_PIX2BYTES(npixels));

  /* Unlock and de-select the device */

  st7789v_deselect(priv);

  return OK;
}

/****************************************************************************
 * Name: st7789v_getrun
 *
 * Description:
 *   This method can be used to read a partial raster line from the LCD.
 *
 * Input Parameters:
 *   row     - Starting row to read from (range: 0 <= row < yres)
 *   col     - Starting column to read from (range: 0 <= col <= xres-npixels)
 *   buffer  - The buffer in which to return the run read from the LCD
 *   npixels - The number of pixels to read from the LCD
 *             (range: 0 < npixels <= xres-col)
 *
 ****************************************************************************/

static int st7789v_getrun(fb_coord_t row, fb_coord_t col,
                          FAR uint8_t *buffer, size_t npixels)
{
#if defined(CONFIG_ST7789V_PARALLEL8BIT) && !defined(CONFIG_LCD_NOGETRUN)
  FAR struct st7789v_dev_s *priv = &g_lcddev;

  /* Sanity check */

  DEBUGASSERT(buffer != NULL && ((uintptr_t)buffer & 1) == 0 &&
              col >= 0 && col+npixels <= ST7789V_XRES &&
              row >= 0 && row < ST7789V_YRES);

  /* Select and lock the device */

  st7789v_select(priv);

  /* Set the starting position for the run */

  st7789v_setcursor(priv, col, row);

  /* Read all of the data */

  st7789v_read(priv, ST7789V_CMD_RAMREAD, buffer,
               ST7789V_PIX2BYTES(npixels));

  /* Unlock and de-select the device */

  st7789v_deselect(priv);

  return OK;
#else
  return -ENOSYS;
#endif
}

/****************************************************************************
 * Name: st7789v_getvideoinfo
 *
 * Description:
 *   Get information about the LCD video controller configuration.
 *
 ****************************************************************************/

static int st7789v_getvideoinfo(FAR struct lcd_dev_s *dev,
                                FAR struct fb_videoinfo_s *vinfo)
{
  DEBUGASSERT(dev != NULL && vinfo != NULL);

  vinfo->fmt     = ST7789V_COLORFMT;
  vinfo->xres    = ST7789V_XRES;
  vinfo->yres    = ST7789V_YRES;
  vinfo->nplanes = 1;

  ginfo("fmt: %u xres: %u yres: %u nplanes: %u\n",
        vinfo->fmt, vinfo->xres, vinfo->yres, vinfo->nplanes);
  return OK;
}

/****************************************************************************
 * Name: st7789v_getplaneinfo
 *
 * Description:
 *   Get information about the configuration of each LCD color plane.
 *
 ****************************************************************************/

static int st7789v_getplaneinfo(FAR struct lcd_dev_s *dev,
                                unsigned int planeno,
                                FAR struct lcd_planeinfo_s *pinfo)
{
  FAR struct st7789v_dev_s *priv = (FAR struct st7789v_dev_s *)dev;

  DEBUGASSERT(dev != NULL && pinfo != NULL && planeno == 0);

  pinfo->putrun = st7789v_putrun;
  pinfo->getrun = st7789v_getrun;
  pinfo->buffer = (uint8_t *)priv->runbuffer;
  pinfo->bpp    = ST7789V_BPP;

  ginfo("planeno: %u bpp: %u\n", planeno, pinfo->bpp);
  return OK;
}

/****************************************************************************
 * Name: st7789v_getpower
 *
 * Description:
 *   Get the LCD panel power status
 *   (0: full off - CONFIG_LCD_MAXPOWER: full on).
 *   On backlit LCDs, this setting may correspond to the backlight setting.
 *
 ****************************************************************************/

static int st7789v_getpower(FAR struct lcd_dev_s *dev)
{
  FAR struct st7789v_dev_s *priv = (FAR struct st7789v_dev_s *)dev;

  /* Sanity check */

  DEBUGASSERT(priv != NULL);
  ginfo("power: %d\n", priv->power);

  return priv->power;
}

/****************************************************************************
 * Name: st7789v_setpower
 *
 * Description:
 *   Enable/disable LCD panel power
 *   (0: full off - CONFIG_LCD_MAXPOWER: full on).
 *   On backlit LCDs, this setting may correspond to the backlight setting.
 *
 ****************************************************************************/

static int st7789v_setpower(FAR struct lcd_dev_s *dev, int power)
{
  FAR struct st7789v_dev_s *priv = (FAR struct st7789v_dev_s *)dev;

  /* Sanity check */

  DEBUGASSERT(priv != NULL && (unsigned int)power <= LCD_FULL_ON);
  ginfo("power: %d\n", power);

  /* Select and lock the device */

  st7789v_select(priv);

  if (power > LCD_FULL_OFF)
    {
      /* Turn the display on */

      st7789v_write(priv, ST7789V_CMD_DISPON, NULL, 0);
      priv->power = LCD_FULL_ON;
    }
  else
    {
      /* Turn the display off */

      st7789v_write(priv, ST7789V_CMD_DISPOFF, NULL, 0);
      priv->power = LCD_FULL_OFF;
    }

  /* Unlock and de-select the device */

  st7789v_deselect(priv);

  return OK;
}

/****************************************************************************
 * Name: st7789v_getcontrast
 *
 * Description:
 *   Get the current contrast setting (0-CONFIG_LCD_MAXCONTRAST).
 *
 ****************************************************************************/

static int st7789v_getcontrast(FAR struct lcd_dev_s *dev)
{
  return -ENOSYS;
}

/****************************************************************************
 * Name: st7789v_setcontrast
 *
 * Description:
 *   Set LCD panel contrast (0-CONFIG_LCD_MAXCONTRAST).
 *
 ****************************************************************************/

static int st7789v_setcontrast(FAR struct lcd_dev_s *dev,
                               unsigned int contrast)
{
  return -ENOSYS;
}

/****************************************************************************
 * Name: st7789v_hwinitialize
 *
 * Description:
 *   Initialize the video hardware.
 *
 ****************************************************************************/

static inline void st7789v_hwinitialize(FAR struct st7789v_dev_s *priv)
{
  uint8_t buf[14];

  /* Select and lock the device */

  st7789v_select(priv);

  /* Memory Data Access Control */
  buf[0] = 0x00;
  st7789v_write(priv, ST7789V_CMD_MADCTL, buf, 1);

  /* RGB 5-6-5-bit  */
  buf[0] = 0x65;
  st7789v_write(priv, ST7789V_CMD_COLMOD, buf, 1);

  /* Porch Setting */
  buf[0] = 0x0C;
  buf[1] = 0x0C;
  buf[2] = 0x00;
  buf[3] = 0x33;
  buf[4] = 0x33;
  st7789v_write(priv, ST7789V_CMD_PORCTRL, buf, 5);

  /*  Gate Control */
  buf[0] = 0x35;
  st7789v_write(priv, ST7789V_CMD_GCTRL, buf, 1);

  /* VCOM Setting */
  buf[0] = 0x19;
  st7789v_write(priv, ST7789V_CMD_VCOMS, buf, 1);

  /* LCM Control */
  buf[0] = 0x2C;
  st7789v_write(priv, ST7789V_CMD_LCMCTRL, buf, 1);

  /* VDV and VRH Command Enable */
  buf[0] = 0x01;
  st7789v_write(priv, ST7789V_CMD_VDVVRHEN, buf, 1);

  /* VRH Set */
  buf[0] = 0x12;
  st7789v_write(priv, ST7789V_CMD_VRHS, buf, 1);

  /* VDV Set */
  buf[0] = 0x20;
  st7789v_write(priv, ST7789V_CMD_VDVS, buf, 1);

  /* Frame Rate Control in Normal Mode */
  buf[0] = 0x0F;
  st7789v_write(priv, ST7789V_CMD_FRCTRL2, buf, 1);

  /* Power Control 1 */
  buf[0] = 0xA4;
  buf[1] = 0xA1;
  st7789v_write(priv, ST7789V_CMD_PWCTRL1, buf, 2);

  /* Positive Voltage Gamma Control */
  buf[0] = 0xD0;
  buf[1] = 0x04;
  buf[2] = 0x0D;
  buf[3] = 0x11;
  buf[4] = 0x13;
  buf[5] = 0x2B;
  buf[6] = 0x3F;
  buf[7] = 0x54;
  buf[8] = 0x4C;
  buf[9] = 0x18;
  buf[10] = 0x0D;
  buf[11] = 0x0B;
  buf[12] = 0x1F;
  buf[13] = 0x23;
  st7789v_write(priv, ST7789V_CMD_PVGAMCTRL, buf, 14);

  /* Negative Voltage Gamma Control */
  buf[0] = 0xD0;
  buf[1] = 0x04;
  buf[2] = 0x0C;
  buf[3] = 0x11;
  buf[4] = 0x13;
  buf[5] = 0x2C;
  buf[6] = 0x3F;
  buf[7] = 0x44;
  buf[8] = 0x51;
  buf[9] = 0x2F;
  buf[10] = 0x1F;
  buf[11] = 0x1F;
  buf[12] = 0x20;
  buf[13] = 0x23;
  st7789v_write(priv, ST7789V_CMD_NVGAMCTRL, buf, 14);

  /* Display Inversion On */
  st7789v_write(priv, ST7789V_CMD_INVON, NULL, 0);

  /* Sleep Out */
  st7789v_write(priv, ST7789V_CMD_SLPOUT, NULL, 0);

  /* wait for power stability */
  up_mdelay(50);

//    lcd_clear(WHITE);

  /* display on */
//    rt_pin_write(LCD_PWR_PIN, PIN_HIGH);
  st7789v_write(priv, ST7789V_CMD_DISPON, NULL, 0);

  /* Unlock and de-select the device */
  st7789v_deselect(priv);
}

/****************************************************************************
 * Name: st7789v_initialize
 *
 * Description:
 *   Initialize the video hardware.  The initial state of the device
 *   is fully initialized, display memory cleared, and ready to use,
 *   but with the power setting at 0 (full off == sleep mode).
 *
 * Input Parameters:
 *   lcd   - A reference to the platform-specific interface.
 *   spi   - A reference to the SPI driver instance.
 *   devno - A value in the range of 0 through CONFIG_ST7789V_NINTERFACES-1.
 *           This allows support for multiple devices.
 *
 * Returned Value:
 *   On success, this function returns a reference to the LCD object for the
 *   specified device.  NULL is returned on failure.
 *
 ****************************************************************************/

#ifdef CONFIG_ST7789V_PARALLEL8BIT
FAR struct lcd_dev_s *st7789v_initialize(FAR struct st7789v_lcd_s *lcd,
                                         unsigned int devno)
#elif defined(CONFIG_ST7789V_SPI3WIRE) || defined(CONFIG_ST7789V_SPI4WIRE)
FAR struct lcd_dev_s *st7789v_initialize(FAR struct spi_dev_s *spi,
                                         unsigned int devno)
#endif
{
  FAR struct st7789v_dev_s *priv = &g_lcddev;

  /* Sanity check */

#ifdef CONFIG_ST7789V_PARALLEL8BIT
  DEBUGASSERT(lcd != NULL);
#elif defined(CONFIG_ST7789V_SPI3WIRE) || defined(CONFIG_ST7789V_SPI4WIRE)
  DEBUGASSERT(spi != NULL);
#endif
  DEBUGASSERT(devno == 0);

  /* Initialize the driver data structure */

  priv->dev.getvideoinfo = st7789v_getvideoinfo;
  priv->dev.getplaneinfo = st7789v_getplaneinfo;
  priv->dev.getpower     = st7789v_getpower;
  priv->dev.setpower     = st7789v_setpower;
  priv->dev.getcontrast  = st7789v_getcontrast;
  priv->dev.setcontrast  = st7789v_setcontrast;
#ifdef CONFIG_ST7789V_PARALLEL8BIT
  priv->lcd              = lcd;
#elif defined(CONFIG_ST7789V_SPI3WIRE) || defined(CONFIG_ST7789V_SPI4WIRE)
  priv->spi              = spi;
#endif
  priv->power            = LCD_FULL_OFF;

  /* Configure the device */

  st7789v_hwinitialize(priv);

  return &priv->dev;
}

#endif /* CONFIG_LCD_ST7789V */
